import { Injectable, Inject } from '@nestjs/common';
import { Order } from 'src/orders/order.entity';
import { OrderItem } from './orderItem.entity';

@Injectable()
export class OrdersItemsService {
 
  async findAll(): Promise<OrderItem[]> {
    return this.ordersItemsRepository.findAll<OrderItem>({
      include: [Order],
    });
  }
}
